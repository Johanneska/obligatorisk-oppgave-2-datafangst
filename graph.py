import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import csv
import numpy as np

# function for calculating moving average
def movingAverage(x, w):
    return np.convolve(x, np.ones(w), 'valid') / w

x = [] # time in ms
y = [] # reading from TCTR1000
t = [] # reading from onewire bus temp sensor
#opens CSV file and split up the content on the , delimiter
with open('data.csv', 'r') as csvfile:
    print("reading csv file....")
    lines = csv.reader(csvfile, delimiter=',')
    for row in lines:
        x.append(row[0])
        y.append(float(row[1]))
        t.append(float(row[2]))



print("csv file has been read, generating graph....")
# creates plots
fig, ax = plt.subplots()
twin1 = ax.twinx()

p1 = ax.plot(y, "pink", label="puls raadata")
p2 = twin1.plot(t, "red", label="Temperatur")
p3 = ax.plot(movingAverage(y, 5),"blue", label = "puls maverage 5")
p4 = ax.plot(movingAverage(y, 3),"green", label = "puls maverage 3") 
ax.set_ylim(0, 2)
# setting labels for graph
ax.set_xlabel("Reading nr")
ax.set_ylabel("Volt")
twin1.set_ylabel("Temperatur")
plt.xticks(rotation = 25)
ax.legend()
twin1.legend()
twin1.yaxis.label.set_color('r')
twin1.tick_params(axis='y', colors='r')
plt.title('Puls og temperatur', fontsize = 20)

# generates a figure and generates a png
figure = plt.gcf() # get current figure
plt.savefig("graph.png", dpi = 300)
print ("Graph generated in file graph.png")
