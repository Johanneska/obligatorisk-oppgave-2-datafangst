# Obligatorisk oppgave 2 - Datafangst

## Selve oppgaven ##

Det skal bygges et sensorsystem som baserer seg på en analog og én digital sensor. Systemet skal kunne visualisere puls ved bruk av en analog sensor på en finger. Systemet skal også visualisere temperatur hentet fra digital sensor.

Følgende komponenter er påkrevd å bruke i oppgaven:

- BeagleBone Black (eller annen BeagleBone som man selv tilpasser oppgaven)
- Digital temperatursensor type DS18B20(+)
- Kombinert infarød lyskilde og fototransistor til avlesing av puls (TCTR1000)
  
## Teori ##
**Hvordan man kan beregne puls ut fra dataene:**

Den analoge sensoren har en IR-lys diode og en sensor som leser hvor mye IR lys som kommer tilbake til sensoren. Måten vi kan måle puls ut ifra dette er at blodet absorberer IR-lys, og vi kan ut fra målingene se når blodet strømmer gjennom fingeren. Det vil si at når blodet strømmer gjennom fingeren vil vi få en lavere verdi, og vi kan telle disse bunn tappene, og dele på tid og vi vil da få et estimat på pulsen vår.

**Presisjonen til systemet:**

Når det gjelder presisjonen til systemet er ikke dette helt optimalt, da systemet ikke er i realtime. Det betyr at selv om jeg i koden prøve å lese av verdien på sensoren vært 10Ms, er det ikke dette som skjer i praksis. Dette kan skyldes alt fra at Multitasking har satt programmet på pause, eller at programmet kanskje venter på diskaksess. Når det kommer til selve målingen vi får ut fra den analoge sensoren, er det også usannsynlig at verdiene er helt optimal, da det ofte finnes mye støy i form av lys osv. dette kan optimaliseres ved å lage bedre filter, enten digitalt eller fysisk. verdiene til den analoge sensoren varier også når på døgnet man leser. Dersom man feks sitter midt på dagen, vil IR lyset fra dagslyset slå ut på sensoren, og føre til mer støy og mer utydelig avlesning og variasjon mellom volt nivået. Det kan derfor være lurt og jobbe på kvelden eller i et mørkt rom. man kan også lage en boks eller lignende som man kan bruke over sensoren for å minimalisere støy. høyden på hvor man har fingeren i forhold til sensoren har også mye og si, da noe av IR lyset reflekteres av fingeren, holder man fingeren helt intill vil man få en lav verdi, da mye av IR lyset blir blokert av fingeren, og ikke når sensoren. holder man fingeren for langt unna, vil man også få en utydelig lesning da ikke nokk IR lys blir absorbert av blodet og det blir umulig å skille mellom puls og vanlig måling, så det gjelder og prøve seg litt fram alt fra noen mm til 1-2 cm over sensoren ser ut til å fungere best.

Et eksempel fra selve dataene:

| timestamp |
| --------  |
|     1     |
|     17    |
|     29    |
|     42    |
|     54    |
|     67    |

Som man kan se er det variasjon mellom lesningene, her er gjennomsnittet mellom lesningene 13,2 ms, noe som viser at systemet ikke er 100% presist.
## Bilder av BBB og sensorne

![image](https://gitlab.com/Johanneska/obligatorisk-oppgave-2-datafangst/-/raw/main/Bilder/IMG_4485.JPEG)
![image](https://gitlab.com/Johanneska/obligatorisk-oppgave-2-datafangst/-/raw/main/Bilder/IMG_4486.JPEG)
![image](https://gitlab.com/Johanneska/obligatorisk-oppgave-2-datafangst/-/raw/main/Bilder/IMG_4488.JPEG)

## Bilder av graf ##

![image](https://gitlab.com/Johanneska/obligatorisk-oppgave-2-datafangst/-/raw/main/Bilder/graf.png)

## Bilder av krets for analog sensor TCTR1000 ##

![image](https://gitlab.com/Johanneska/obligatorisk-oppgave-2-datafangst/-/raw/main/Bilder/AnalogKrets.png)
![image](https://gitlab.com/Johanneska/obligatorisk-oppgave-2-datafangst/-/raw/main/Bilder/AnalogKrets2.png)

## Bilder av krets for digital sensor DS18B20 ##
![image](https://gitlab.com/Johanneska/obligatorisk-oppgave-2-datafangst/-/raw/main/Bilder/DigitalKrets.png)
