import Adafruit_BBIO.ADC as ADC
import csv
import time
import thread
import multiprocessing as mp
from time import sleep

f = open('data.csv', 'w') # opens CSV in write mode
writer = csv.writer(f)
ADC.setup()
analogPin ="AIN1" #analog pin to read from
w1="/sys/bus/w1/devices/28-00000bc4aad3/w1_slave" #one wire bus

#function for reading temperature on the 1 wire bus
def read_temperature(return_value):
        while True:
                raw = open(w1, "r").read()
                # splits the return value and extracts the temp value / 1000
                temp = float(raw.split("t=")[-1])/1000
                return_value.value = temp

if __name__ == '__main__':
        #multi prossesing to read from both one wire and the analog sensor
        temperature = mp.Value("d", 0.0, lock=False)
        readTemp = mp.Process(target=read_temperature, args=(temperature,))
        readTemp.start()
        #timer for keeping time between readings
        start = round(time.time() * 1000)
        while True:
                value=ADC.read(analogPin) #reads value from analogpin
                temp =  temperature.value
                timer = int(round(time.time() * 1000) - start)
                #writes values to CSV file fo later use in graph.py
                writer.writerow([timer, value * 1.8, temp])
                print timer, value * 1.8, temp
                sleep(.01)
